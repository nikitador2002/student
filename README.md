# Backend student (educational project)

## Project stack:

- PHP (Symfony)
- Docker
- PostgreSQL
- Nginx

## Creating your repository

- Copying the project from the repository `git clone -b main path_to_repository new_project_name`
- Go to project directory `cd new_project_name`
- Remove .git directory `rm -rf .git`
- Git initialize `git init --initial-branch=main`
- Create your repository in GitLab
- Linking to the new repository `git remote add origin path_to_your_repository`

## Project deployment

- `docker-compose pull` — download all necessary Docker images
- `docker-compose build` — build local images
- `docker-compose up -d` — start containers.

As a result, you will have a bunch of containers with working application. Remember that **all commands must be run inside the `php` container**. In other words, you SHOULD NOT run Symfony console like `bin/console`, but only `docker-compose exec app bin/console`.

Next steps:

- place `.env.local` file with project variables to root project directory.
- install dependencies: `docker-compose exec php composer install`
- apply database migrations: `docker-compose exec php bin/console doctrine:migrations:migrate`

## Reset application to zero state

- `doctrine:database:drop --force` will delete the whole database;
- `doctrine:database:create` will create empty database;
- `doctrine:migrations:migrate` will apply migrations

You can apply fixtures with `doctrine:fixtures:load` command. Remember that this command will **DROP ALL EXISTING DATA** from your local database.
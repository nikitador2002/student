FROM composer:latest as composer
FROM php:8.1.5-fpm-alpine as php

RUN set -xe && apk update && apk upgrade

RUN set -xe \
    && apk add --no-cache \
       shadow \
       postgresql-dev \
    && docker-php-ext-install \
       opcache \
       pdo_pgsql

COPY --from=composer /usr/bin/composer /usr/local/bin/composer
RUN /usr/local/bin/composer self-update

WORKDIR /var/www/app
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV COMPOSER_MEMORY_LIMIT=-1

ARG UID
ARG GID
ENV TARGET_UID ${UID:-1000}
ENV TARGET_GID ${GID:-1000}

RUN usermod -u ${TARGET_UID} www-data && groupmod -g ${TARGET_GID} www-data
RUN mkdir -p /var/www/app && chown -R www-data:www-data /var/www/app
